import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'titleName'
})

export class TitleNamePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let filteredValue = value.toLowerCase().split(' ');
    if (filteredValue == "") {
      return "";
    }
    for (let i = 0; i < filteredValue.length; i++) {
      filteredValue[i] = filteredValue[i].replace(/[^a-z]/g, '');
      filteredValue[i] = filteredValue[i][0].toUpperCase() + filteredValue[i].substr(1, filteredValue[i].length);
    }
    filteredValue = filteredValue.join(' ');
    return filteredValue;
  }

}
