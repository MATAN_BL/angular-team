import {Component} from '@angular/core';
import {DatePipe} from '@angular/common';
import {Http, Response} from "@angular/http";
import {TitleNamePipe} from "./title-name.pipe";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  http: Http;
  listOfBooks: Object[];

  constructor(http: Http, private pipe: TitleNamePipe, private datePipe: DatePipe) {
    this.http = http;
    this.http.request('http://localhost:4200/db/books').subscribe((res: Response) => {
      this.listOfBooks = res.json();
      this.listOfBooks.forEach((book) => {
        book["title"] = this.pipe.transform(book["title"]);
      });
      console.log(this.listOfBooks);
    })
  }

  changeTitle(titleElem: HTMLElement, item: Object) {
      let newString = this.pipe.transform(titleElem.textContent);
      titleElem.textContent = newString;
      let oldString = item["title"];
      this.validateEmptyString(newString, oldString, titleElem);
  }

  validateEmptyString(newText: string, oldString: string, elem: HTMLElement) {
    if (newText == "") {
      elem.textContent = oldString;
    }
  }

  validateDate(dateElement: HTMLElement, item: Object) {
    if ((new Date(dateElement.textContent)).toDateString() == "Invalid Date") {
      dateElement.textContent = this.datePipe.transform(item["date"]);
    }
  }

  deleteItem(item: Object) {
    this.listOfBooks = this.listOfBooks.filter((book) => {
      return !(JSON.stringify(book) == JSON.stringify(item));
    })
  }

}
