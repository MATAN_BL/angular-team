import { BrowserModule } from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { TitleNamePipe } from './title-name.pipe';
import {DatePipe} from "@angular/common";


@NgModule({
  declarations: [
    AppComponent,
    TitleNamePipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [TitleNamePipe, DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
