import { AngularTeamPage } from './app.po';

describe('angular-team App', function() {
  let page: AngularTeamPage;

  beforeEach(() => {
    page = new AngularTeamPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
